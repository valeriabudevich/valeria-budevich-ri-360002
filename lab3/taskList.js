import React from "react";

export default class List extends React.Component {
  remove() {
    let removeItem = this.props.list;
    this.props.remove(removeItem);
  }
  edit() {
    let editItem = this.props.list;
    this.props.edit(editItem);
  }

  render(props) {
    return (
      <div>
        <table className="list">
          <td>
            <tr>
              <strong>Title : </strong>
            </tr>

            <tr>
              <strong>Description : </strong>
            </tr>

            <tr>
              <strong>Date : </strong>
            </tr>
          </td>
          <td>
            <tr>{this.props.list.taskTitle}</tr>
            <tr>{this.props.list.taskDescription}</tr>
            <tr>{this.props.list.date}</tr>
          </td>
          <button className="button2" onClick={this.remove.bind(this)}>
            x
          </button>
          <button className="button3" onClick={this.edit.bind(this)}>
            <img
              src="https://cdn.onlinewebfonts.com/svg/img_194863.png"
              alt=""
              width="10"
              height="10"
            />
          </button>
        </table>
      </div>
    );
  }
}
