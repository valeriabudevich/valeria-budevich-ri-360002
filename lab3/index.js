import React from "react";
import { render } from "react-dom";
import Header from "./header";
import Result from "./result";
import TaskCreator from "./taskCreator";
import "./styles.css";

class App extends React.Component {
  state = { taskList: [] };

  handleItem(title) {
    let list = this.state.taskList;
    list.push(title);
    this.setState({ taskList: list });
  }

  removeItem(title) {
    let list = this.state.taskList;
    let index = list.indexOf(title);
    list.splice(index, 1);
    this.setState({ taskList: list });
  }

  editItem(title) {
    //Реализовать редактор
    let list = this.state.taskList;
    let index = list.indexOf(title);
  }

  render() {
    return (
      <div>
        <Header />
        <center className="box">
          <TaskCreator addTask={this.handleItem.bind(this)} />
        </center>
        <Result
          list={this.state.taskList}
          remove={this.removeItem.bind(this)}
          edit={this.editItem.bind(this)}
        />
      </div>
    );
  }
}

render(<App />, document.getElementById("root"));
