import React from "react";
import List from "./taskList";

export default class Result extends React.Component {
  removeItem(item) {
    this.props.remove(item);
  }
  editItem(item) {
    this.props.edit(item);
  }

  render(props) {
    let items;
    items = this.props.list.map(item => {
      return (
        <List
          key={item.title}
          list={item}
          remove={this.removeItem.bind(this)}
          edit={this.editItem.bind(this)}
        />
      );
    });

    return (
      <div className="fullList">
        <div>
          <center>
            <h3 className="header1">Tasks</h3>
          </center>
          {items}
        </div>
      </div>
    );
  }
}
