const config = require('../config');
const { getUserInfo, userSignIn, userSignUp, userUpdate, isLoggedToday } = require('./user_workers');
const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(config.token, {polling: true});
const bot_msgs = require('./bot_msgs');
//  t.me/L5AuthBot

bot.onText(/\/start/, (msg) => {
    bot.sendMessage(msg.chat.id, bot_msgs.start);
});

bot.onText(/\/getUserInfo/, (msg, match) => {
    const id = match.input.split(' ')[1];
    if(!id)
      bot.sendMessage(msg.chat.id, bot_msgs.userInfo);
    else
      getUserInfo(id, (res) => bot.sendMessage(msg.chat.id, res));
});

bot.onText(/\/signIn/, (msg, match) => {
    const username = match.input.split(' ')[1];
    const password = match.input.split(' ')[2];
    if (!username || !password) 
      bot.sendMessage(msg.chat.id, bot_msgs.signIn);
    else
      userSignIn(username, password, (res) => bot.sendMessage(msg.chat.id, res));
});

bot.onText(/\/signUp/, (msg, match) => {
  const regInfo = match.input.split('\n');
  let info = [regInfo[1], regInfo[2], regInfo[3],regInfo[4],regInfo[5], regInfo[6]];
  if(!info[0] || !info[1])
    bot.sendMessage(msg.chat.id, bot_msgs.signUp)
  else
    userSignUp(info, (res) => bot.sendMessage(msg.chat.id, res));
});

bot.onText(/\/userUpdate/, (msg, match) => {
  const regInfo = match.input.split('\n');
  let userUpd = {};
  regInfo.forEach((element, i) => {
    if(i != 0){
      userUpd[element.split(':')[0]] = element.split(':')[1]
    }
  });
  if(!userUpd.id)
    bot.sendMessage(msg.chat.id, bot_msgs.userUpd)
  else
    userUpdate(userUpd, (res) => bot.sendMessage(msg.chat.id, res));
});

bot.onText(/\/isLoggedToday/, (msg, match) => {
    const id = match.input.split(' ')[1];
    if(!id)
      bot.sendMessage(msg.chat.id, bot_msgs.isLogged);
    else
      isLoggedToday(id, (res) => bot.sendMessage(msg.chat.id, res));
});