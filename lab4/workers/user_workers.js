const User = require('../models/user');

module.exports = {
    userSignUp,
    getUserInfo,
    userSignIn,
    userUpdate,
    isLoggedToday
};

function userSignUp(req, res, next) {
    let { username, password, firstname, middlename, lastname, birthday } = req.body;
    if (!username) return res.json(new Error('Нет обязательных полей!'));
    User.findOne({ userlogin: username }, (err, user) => {
        if (err) return res.json(new Error(err));
        if (user) {
            res.json(new Error('Такой пользователь уже существует!'));
        } else {
            let newUser = new User();
            newUser.userlogin = username;
            newUser.password = password;
            newUser.firstName = firstname;
            newUser.middleName = middlename;
            newUser.lastName = lastname;
            newUser.birthdate = birthday;
            newUser.save((err, doc) => {
                if (err) return res.json({ success: false, error: err });
                return res.json(doc);
            });
        }
    });
}

function getUserInfo(req, res, next) {
    let { userID } = req.body;
    if (!userID) return res.json(new Error('Нет ID'));
    User.findById(userID, (err, user) => {
        if (err) return res.json(new Error(err));
        res.json(user);
    });
}

function userSignIn(req, res, next) {
    let { username, password } = req.body;
    if (!username) return res.json(new Error('Отсутствует логин'));
    if (!password) return res.json(new Error('Отсутствует пароль'));
    User.findOne({ userlogin: username }, (err, user) => {
        if (err) return res.json(new Error(err));
        if (!user) {
            return res.json(new Error('Такого пользователя не существует'));
        } else if (password != user.password) {
            return res.json(new Error('Неправильный пароль'));
        } else {
            let today = new Date();
            user.lastlogin = `${today.getDate()}.${(today.getMonth() + 1)}.${today.getFullYear()}`;
            user.save();
            return res.json(user);
        }
    })
}

function userUpdate(req, res, next){
    let {userID, username, password, firstname, middlename, lastname, birthday } = req.body;
    if (!userID) return res.json(new Error('Нет ID'));
    User.findById(userID, (err, user) => {
        if(err) return res.json(new Error(err));
        if(!user) return res.json(new Error('Пользователь не найден'));
        if(username) user.userlogin = username;
        if(password) user.password = password;
        if(firstname) user.firstName = firstname;
        if(middlename) user.middleName = middlename;
        if(lastname) user.lastName = username; 
        if(birthday) user.birthdate = birthday;
        user.save();
        return res.json(user); 
    });
}

function isLoggedToday(req, res, next) {
    let { userID } = req.body;
    if (!userID) return res.json(new Error('Нет ID'));
    User.findById(userID, (err, user) => {
        if(err) return res.json(new Error(err));
        if(!user) return res.json(new Error('Пользователь не найден'));
        if(!user.lastlogin) return res.json(new Error('Данные отсутствуют'));
        else {
            let today = new Date();
            return res.json(user.lastlogin === `${today.getDate()}.${(today.getMonth() + 1)}.${today.getFullYear()}`);
        }
    })    
}